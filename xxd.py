#!/usr/bin/env python3

import sys
import re

MAX_LINE_LENGTH = 80

def is_printable_ascii(byte):
    return byte >= ord(' ') and byte <= ord('~')

def needs_escaping(byte):
    return byte == ord('\"') or byte == ord('\\')

def stringify_nibble(nibble):
    if nibble < 10:
        return chr(nibble + ord('0'))
    return chr(nibble - 10 + ord('a'))

def write_byte(of, byte):
    if is_printable_ascii(byte):
        if needs_escaping(byte):
            of.write('\\')
        of.write(chr(byte))
    else:
        of.write('\\x')
        of.write(stringify_nibble(byte >> 4))
        of.write(stringify_nibble(byte & 0xf))

def mk_valid_identifier(s):
    s = re.sub('^[^_a-z]', '_', s)
    s = re.sub('[^_a-z0-9]', '_', s)
    return s

def main():
    identifier = mk_valid_identifier(sys.argv[1]);
    sys.stdout.write('#include <stddef.h>\n\n');
    sys.stdout.write('const char {}[] =\n"'.format(identifier));

    line_length = 0
    while True:
        char = sys.stdin.read(1)
        if char == '':
            break
        elif char == '\n':
            line_length = 0
            sys.stdout.write('\\n"\n"')
        elif line_length + 2 > MAX_LINE_LENGTH:
            line_length = 0
            sys.stdout.write('"\n"')
        else:
            write_byte(sys.stdout, ord(char))
            line_length += 1

    sys.stdout.write('";\n\n');
    sys.stdout.write('const size_t {}_len = sizeof({}) - 1;\n'.format(identifier, identifier));

if __name__ == '__main__':
    main()

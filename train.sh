#!/bin/bash

set -e
set -o xtrace
cd "${0%/*}"

# Images should be in dataset/

echo Preparing RAVU dataset...
mkdir -p mpv-prescalers/dataset
for i in dataset/*.png; do
    out="mpv-prescalers/dataset/$(basename "$i" .png).pnm"
    [[ -f "$out" ]] || convert -depth 16 "$i" -colorspace gray -compress none "$out"
done

echo Generating RAVU model
cd mpv-prescalers
git checkout ravu
make ravu
./ravu calerrain dataset/*.pnm > ../ravu_weights-r3.py
git checkout source

echo Generating RAVU shader
./ravu.py -t luma -w ../ravu_weights-r3.py --max-downscaling-ratio=1.414213 --float-format=float16vk --use-compute-shader > ../ravu-r3-compute.glsl
cd ..

echo Training FSRCNNX model
cd FSRCNN-TensorFlow
python3 ./main.py --data_dir ../dataset --distort True
python3 ./main.py --params True

echo Generating FSRCNNX shader
cd params
python3 ../gen.py weights8_0_4_1.txt
mv FSRCNNX_x2_8-0-4-1.glsl ../../
cd ../..

echo Formatting output shaders
mkdir -p out
gen() {
    grep -E '^[^/]|^//!' | ./xxd.py $1 >> out/$1.c
}

cat > out/ravu_r3_compute.c <<EOF
/*****************************************************************************
 * RAVU r3, luma-only compute variant
 *****************************************************************************
 * Copyright (C) Bin Jin 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#include "shaders.h"
EOF
gen ravu_r3_compute < ravu-r3-compute.glsl

cat > out/fsrcnnx_8_0_4_1.c <<EOF
/*****************************************************************************
 * FSRCNN-X, based on FSRCNN-TensorFlow
 *****************************************************************************
 * Copyright (c) 2016 Drake Levy
 * Copyright (c) 2017 Niklas Haas
 * Copyright (c) 2017 igv
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *****************************************************************************/

#include "shaders.h"
EOF
gen fsrcnnx_8_0_4_1 < FSRCNNX_x2_8-0-4-1.glsl

cat > out/krig_bilateral.c <<EOF
/*****************************************************************************
 * KrigBilateral by Shiandow, adapted for mpv by igv
 *****************************************************************************
 * Copyright (C) 2016 Shiandow
 * Copyright (C) 2020 igv
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 3.0 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.
 *****************************************************************************/

#include "shaders.h"
EOF
gen krig_bilateral < krig-bilateral/KrigBilateral.glsl

cat > out/ssim_super_res.c <<EOF
/*****************************************************************************
 * SSimSuperRes by Shiandow, adapted for mpv by igv
 *****************************************************************************
 * Copyright (C) 2016 Shiandow
 * Copyright (C) 2020 igv
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 3.0 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.
 *****************************************************************************/

#include "shaders.h"
EOF
gen ssim_super_res < ssimsuperres/SSimSuperRes.glsl


cat > out/ssim_downscaler.c <<EOF
/*****************************************************************************
 * SSimDownscaler by Shiandow, adapted for mpv by igv
 *****************************************************************************
 * Copyright (C) 2017 Shiandow
 * Copyright (C) 2020 igv
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 3.0 of the License, or (at your
 * option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.
 *****************************************************************************/

#include "shaders.h"
EOF
gen ssim_downscaler < ssimdownscaler/SSimDownscaler.glsl
